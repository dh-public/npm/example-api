import createServer from './server'
import { AppDataSource } from './data-source'

const port = process.env.PORT || 4000


const app = createServer()

try {
  app.listen(port, async () => {
    await AppDataSource.initialize()
    // bad for production
    await AppDataSource.synchronize()

    console.log(`successfully creates connections`)

  })
} catch (err) {
  console.log(`Error: Unable to create user: ${(err as Error).message}. ${(err as Error).stack}`)
}

export { app }




