import express from 'express'
import UserController from '../controllers/userController'

const router = express.Router()

router.post(
  '/', [], [UserController.createUser.bind(UserController)]
)

router.get(
  '/:id', [], [UserController.getUserById.bind(UserController)]
)

router.put(
  '/:id', [], [UserController.editUserById.bind(UserController)]
)

router.delete(
  '/:id', [], [UserController.deleteUserById.bind(UserController)]
)

export default router
