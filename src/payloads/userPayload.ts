
export interface UserPayload {
    firstName: string
    lastName: string
    age: number
}