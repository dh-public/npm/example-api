import 'reflect-metadata'
import { DataSource } from 'typeorm'
import { User } from './entity/User'
import { injectable } from 'inversify'

export const AppDataSource = new DataSource({
  type: 'mariadb',
  // from docker file
  host: 'database',
  port: 3306,
  username: 'root',
  password: 'root',
  database: 'test_dbAC',
  synchronize: true,
  logging: false,
  entities: [User],
  migrations: [],
  subscribers: [],
})
