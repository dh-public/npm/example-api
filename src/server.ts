import express, { Application } from 'express'
import user from './routers/userRouter'
import cors from 'cors'
import bodyParser from 'body-parser'

export default function createServer() {
  const app: Application = express()

  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json())
  app.use(cors())
  app.use('/users', user)

  return app
}