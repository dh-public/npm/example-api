import { User } from '../entity/User'
import { UserPayload } from '../payloads/userPayload'
import { AppDataSource } from '../data-source'

export default class UserService {
  async saveUser (userData: User | UserPayload): Promise<User> {
    const userRepo = await AppDataSource.getRepository(User)
    return userRepo.save(userData)
  }

  async getUserById (Id: number) {
    const userRepo = await AppDataSource.getRepository(User)
    return userRepo.findOne({ where: { id: Id , deleted: false } })
  }

  async updateUserById(userId: number, userPayload: UserPayload) {
    const userRepo = await AppDataSource.getRepository(User)
    const user = await this.getUserById(userId)

    if(!user) {
      return user
    }

    user.firstName = userPayload.firstName
    user.lastName = userPayload.lastName
    user.age = userPayload.age

    return userRepo.save(user)
  }

  async deleteUserById(userId: number) {
    const userRepo = await AppDataSource.getRepository(User)
    const user = await this.getUserById(userId)

    if(!user) {
      return user
    }

    user.deleted = true

    return userRepo.save(user)
  }
}