import { UserPayload } from '../payloads/userPayload'
import { Request, Response } from 'express'
import UserService from '../services/userService'
import { User } from '../entity/User'

export default class UserController {
  private static userService: UserService = new UserService()

  public static async createUser(req: Request, res: Response) {
    const userPayload: UserPayload = req.body as UserPayload

    const newUser = await this.userService.saveUser(userPayload)

    await this.returnUser(newUser, res)
  }

  public static async getUserById(req: Request, res: Response) {
    const id: number = parseInt(req.params.id, 10)

    const user = await this.userService.getUserById(id)

    await this.returnUser(user, res)
  }

  public static async editUserById(req: Request, res: Response) {
    const id: number = parseInt(req.params.id, 10)
    const userPayload: UserPayload = req.body as UserPayload


    const user = await this.userService.updateUserById(id, userPayload)

    await this.returnUser(user, res)
  }

  public static async deleteUserById(req: Request, res: Response) {
    const id: number = parseInt(req.params.id, 10)

    const user = await this.userService.deleteUserById(id)

    await this.returnUser(user, res)
  }

  private static async returnUser(user: User | null, res: Response) {
    if(!user) {
      res.status(404)
      res.send('user not found')
    }

    res.status(200)
    res.send(user)
  }
}