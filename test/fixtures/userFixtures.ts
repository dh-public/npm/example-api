import userFixtureData from './fixture-data/userFixtureData.json'
import { User } from "../../src/entity/User"
import { Repository } from "typeorm"
import { AppDataSource } from "../../src/data-source"


export default class UserFixtures {
    public Data: User[] = userFixtureData

    private repository: Repository<User>

    public async StartUp() {
        this.repository = await AppDataSource.getRepository(User)
        await this.repository.save(this.Data)
    }

    public async TearDown() {
        await this.repository.clear()
    }
}