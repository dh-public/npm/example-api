import UserFixtures from "./fixtures/userFixtures"
import { AppDataSource } from "../src/data-source"

export default class Fixture {

    public users: UserFixtures = new UserFixtures()


    public async init(): Promise<void> {
        await AppDataSource.initialize()
        await this.users.StartUp()
    }

    public async end(): Promise<void> {
        await this.users.TearDown()
        await AppDataSource.destroy()
    }
}