import chai from 'chai'
import chaiHttp = require('chai-http')
import createServer from '../../../src/server'
import Fixture from '../../fixture'

chai.use(chaiHttp)

describe('Delete Users', async () => {
  describe('Delete User By Id', async () => {
    const expect = chai.expect
    const app = createServer()
    const fixtures: Fixture = new Fixture()


    before(async () => {
      await fixtures.init()
    })

    after(async () => {
      await fixtures.end()
    })

    async function DeleteUserByIdRequest(userId: number) {
      return chai.request(app)
        .delete(`/users/${userId}`)
    }


    it('should return 200 and user when get', async () => {
      const res = await DeleteUserByIdRequest(fixtures.users.Data[0].id)

      expect(res.status).eq(200)

      expect(res.body.id).eq(1)
      expect(res.body.firstName).eq('bill')
      expect(res.body.lastName).eq('gates')
      expect(res.body.age).eq(25)
      expect(res.body.deleted).eq(true)
    })
  })
})