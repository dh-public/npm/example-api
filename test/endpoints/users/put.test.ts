import chai, { expect } from 'chai'
import chaiHttp = require('chai-http')
import createServer from '../../../src/server'
import { AppDataSource } from "../../../src/data-source"
import { UserPayload } from "../../../src/payloads/userPayload"
import Fixture from "../../fixture"

chai.use(chaiHttp)


describe('Edit User', async () => {
    describe('Edit User By ID', async () => {
        const app = createServer()
        const fixtures: Fixture = new Fixture()

        before(async () => {
            await fixtures.init()
        })

        after(async () => {
            await fixtures.end()
        })

        async function UpdateUserByIdRequest(userId: number, userPayload: UserPayload) {
            return chai.request(app)
                .put(`/users/${userId}`)
                .send(userPayload)
        }


        it('should return 200 and user when put', async () => {
            const userPayload: UserPayload = {
                firstName: "dave",
                lastName: "plumber",
                age: 26
            }

            const res = await UpdateUserByIdRequest(1, userPayload)

            expect(res.status).eq(200)

            expect(res.body.firstName).eq('dave')
            expect(res.body.lastName).eq('plumber')
            expect(res.body.age).eq(26)
        })
    })
})