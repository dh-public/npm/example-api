import chai from 'chai'
import chaiHttp = require('chai-http')
import createServer from '../../../src/server'
import Fixture from "../../fixture"

chai.use(chaiHttp)

describe('Get Users', async () => {
    describe('Get User By Id', async () => {
        const expect = chai.expect
        const app = createServer()
        const fixtures: Fixture = new Fixture()


        before(async () => {
            await fixtures.init()
        })

        after(async () => {
            await fixtures.end()
        })

        async function GetUserByIdRequest(userId: number) {
            return chai.request(app)
                .get(`/users/${userId}`)
        }


        it('should return 200 and user when get', async () => {
            const res = await GetUserByIdRequest(1)

            expect(res.status).eq(200)

            expect(res.body.id).eq(1)
            expect(res.body.firstName).eq('bill')
            expect(res.body.lastName).eq('gates')
            expect(res.body.age).eq(25)
            expect(res.body.deleted).eq(false)
        })
    })
})