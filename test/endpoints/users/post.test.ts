import chai, { expect } from 'chai'
import chaiHttp = require('chai-http')
import createServer from '../../../src/server'
import { AppDataSource } from '../../../src/data-source'
import { UserPayload } from '../../../src/payloads/userPayload'
import Fixture from '../../fixture'

chai.use(chaiHttp)


describe('Create User', async () => {
  const app = createServer()
  const fixtures = new Fixture()

  before( async () => {
    await fixtures.init()
  })

  after(async () => {
    await fixtures.end()
  })

  async function CreateUserRequest(userPayload: UserPayload) {
    return chai.request(app)
      .post('/users')
      .send(userPayload)
  }


  it('should return 200 and user when post', async () => {
    const userPayload: UserPayload = {
      firstName: 'bill',
      lastName: 'gates',
      age: 26
    }

    const res = await CreateUserRequest(userPayload)

    expect(res.status).eq(200)

    expect(res.body.id).eq(6)
    expect(res.body.firstName).eq('bill')
    expect(res.body.lastName).eq('gates')
    expect(res.body.age).eq(26)
  })
})